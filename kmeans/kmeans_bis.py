import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import scipy.stats as stats



# Load the data
df = pd.read_csv('data.csv',sep=',')
# réctifier les noms des colonnes
df.columns = df.columns.str.strip()
df=df.drop(columns=['Net Income Flag'],axis=1) # Drop the column with only one value

## ignorer #####
Only_categorigal_column=df['Liability-Assets Flag'].copy()
###############


df=df.drop(columns=['Liability-Assets Flag'],axis=1) # Supprimer cette variable qualitative car elle n'apporte pas d'information utile.

# normaliser les données
original_brankrupt = df['Bankrupt?']
original_df_X=df.drop(columns=['Bankrupt?']).copy()
scaler = StandardScaler()
df = pd.DataFrame(scaler.fit_transform(df), columns=df.columns)

#### ignorer ######
df_X = df.drop(columns=['Bankrupt?'])
df_y = df['Bankrupt?']
####################

strong_corr_threshold = 0.7
pd_corr=df_X.corr()
np.fill_diagonal(pd_corr.values, 0)
high_corr=pd_corr[abs(pd_corr)>strong_corr_threshold]
#high_corr=high_corr.stack().reset_index()
#high_corr=high_corr.rename(columns={'level_0':'Feature 1','level_1':'Feature 2',0:'Correlation Coefficient'})

list_high_corr=[]
for i in range(len(high_corr)):
    tempo_list=[]
    tempo_list.append(high_corr.index[i])
    for j in range(len(high_corr.columns)):
        if abs(high_corr.iloc[i,j])>strong_corr_threshold:
            tempo_list.append(high_corr.columns[j])
    if len(tempo_list)>1:
        list_high_corr.append(tempo_list)

from collections import defaultdict, deque

strong_corr_threshold = 0.7
pd_corr = df_X.corr()
np.fill_diagonal(pd_corr.values, 0)
high_corr = pd_corr[abs(pd_corr) > strong_corr_threshold]
# high_corr=high_corr.stack().reset_index()
# high_corr=high_corr.rename(columns={'level_0':'Feature 1','level_1':'Feature 2',0:'Correlation Coefficient'})

list_high_corr = []
for i in range(len(high_corr)):
    tempo_list = []
    tempo_list.append(high_corr.index[i])
    for j in range(len(high_corr.columns)):
        if abs(high_corr.iloc[i, j]) > strong_corr_threshold:
            tempo_list.append(high_corr.columns[j])
    if len(tempo_list) > 1:
        list_high_corr.append(tempo_list)


def group_lists(lists):
    # graph
    element_to_list_ids = defaultdict(set)
    list_id_to_elements = {}

    for i, lst in enumerate(lists):
        list_id_to_elements[i] = set(lst)
        for element in lst:
            element_to_list_ids[element].add(i)

    # BFS
    visited = set()

    def bfs(start):
        queue = deque([start])
        component = []
        while queue:
            node = queue.popleft()
            if node not in visited:
                visited.add(node)
                component.append(node)
                for element in list_id_to_elements[node]:
                    for neighbor in element_to_list_ids[element]:
                        if neighbor not in visited:
                            queue.append(neighbor)
        return component

    components = []
    for list_id in range(len(lists)):
        if list_id not in visited:
            components.append(bfs(list_id))

    # regroupement des listes
    grouped_lists = []
    for component in components:
        combined_set = set()
        for list_id in component:
            combined_set.update(list_id_to_elements[list_id])
        grouped_lists.append(list(combined_set))

    return grouped_lists


grouped = group_lists(list_high_corr)

columns_to_keep=['Net Income to Total Assets',
 'Gross Profit to Sales',
 'Operating Profit Rate',
 'Cash Flow to Sales',
 'Cash flow rate',
 'Regular Net Profit Growth Rate',
 'Net worth/Assets',
 'Liability to Equity',
 'Working Capital/Equity',
 'Total Asset Turnover',
 'Current Assets/Total Assets',
 'Current Liability to Liability',
 'Cash Flow to Total Assets'
]
merged_columns = [x for sublist in grouped for x in sublist]

# columns to keep :
#columns_to_keep=["Net Value Per Share (A)","Gross Profit to Sales","After-tax Net Profit Growth Rate","Persistent EPS in the Last Four Seasons","Debt ratio %",
#                "After-tax net Interest Rate",'Operating Funds to Liability','Net Income to Total Assets','Cash Flow to Sales','Liability to Equity','Current Liability to Liability']
columns_nonCorrelated=[x for x in list(df_X.columns) if x not in merged_columns]
columns_nonCorrelated.extend(columns_to_keep)
df_X=df_X[columns_nonCorrelated]
df_X['Bankrupt?'] = original_brankrupt
print(df_X)
#############################################################################
#############################################################################
#############################################################################

#Pour garder seulement les variables un minimum correlées
for columnName in df_X:
    if columnName != 'Bankrupt?':
        new_data = pd.DataFrame(df[['Bankrupt?', columnName]])
        # calculate point-biserial correlation
        x = new_data['Bankrupt?'].copy()
        y = new_data[columnName].copy()
        result = stats.pointbiserialr(x, y)
        print(result)
        if 0.09 >= result.statistic >= -0.09:
            df_X = df_X.drop(columns=[columnName],axis=1)
        else :
            print(result.statistic)
            print(columnName)
print(df_X)



def scatterplot_pca(
    columns=None, hue=None, style=None, data=None, pc1=1, pc2=2, **kwargs
):
    """Diagramme de dispersion dans le premier plan principal.

    Permet d'afficher un diagramme de dispersion lorsque les données
    ont plus de deux dimensions. L'argument `columns` spécifie la
    liste des colonnes à utiliser pour la PCA dans le jeu de données
    `data`. Les arguments `style` et `hue` permettent de spécifier la
    forme et la couleur des marqueurs. Les arguments `pc1` et `pc2`
    permettent de sélectionner les composantes principales (par défaut
    la première et deuxième). Retourne l'objet `Axes` ainsi que le
    modèle `PCA` utilisé pour réduire la dimension.

    :param columns: Les colonnes quantitatives de `data` à utiliser
    :param hue: La colonne de coloration
    :param style: La colonne du style
    :param data: Le dataFrame Pandas
    :param pc1: La composante en abscisse
    :param pc2: La composante en ordonnée

    """
     # Select relevant columns (should be numeric)
    data_quant = data if columns is None else data[columns]
    data_quant = data_quant.drop(
        columns=[e for e in [hue, style] if e is not None], errors="ignore"
    )

    # Reduce to two dimensions if needed
    if data_quant.shape[1] == 2:
        data_pca = data_quant
        pca = None
    else:
        n_components = max(pc1, pc2)
        pca = PCA(n_components=n_components)
        data_pca = pca.fit_transform(data_quant)
        data_pca = pd.DataFrame(
            data_pca[:, [pc1 - 1, pc2 - 1]], columns=[f"PC{pc1}", f"PC{pc2}"]
        )

    # Keep name, force categorical data for hue and steal index to
    # avoid unwanted alignment
    if isinstance(hue, pd.Series):
        if not hue.name:
            hue.name = "hue"
        hue_name = hue.name
    elif isinstance(hue, str):
        hue_name = hue
        hue = data[hue]
    elif isinstance(hue, np.ndarray):
        hue = pd.Series(hue, name="class")
        hue_name = "class"

    hue = hue.astype("category")
    hue.index = data_pca.index
    hue.name = hue_name

    if isinstance(style, pd.Series):
        if not style.name:
            style.name = "style"
        style_name = style.name
    elif isinstance(style, str):
        style_name = style
        style = data[style]
    elif isinstance(style, np.ndarray):
        style = pd.Series(style, name="style")
        style_name = "style"

    full_data = data_pca
    if hue is not None:
        full_data = pd.concat((full_data, hue), axis=1)
        kwargs["hue"] = hue_name
    if style is not None:
        full_data = pd.concat((full_data, style), axis=1)
        kwargs["style"] = style_name

    x, y = data_pca.columns
    ax = sns.scatterplot(x=x, y=y, data=full_data, **kwargs)

    return ax, pca



nombre_clusters_kmeans = 3
cls = KMeans(n_clusters=nombre_clusters_kmeans, init="random")
df_X0 = df_X.drop(columns=['Bankrupt?'])
cls.fit(df_X0)
labels_kmeans = pd.Series(cls.labels_, name="K-means")
df_X["K-means"] = labels_kmeans
scatterplot_pca(data=df_X0, hue=labels_kmeans, style=df_X['Bankrupt?'])
plt.show()

print("Pourcentage d'entreprises en faillites dans même cluster : ")
df_test = df_X[df_X["Bankrupt?"] == 1]
for i in range(0, nombre_clusters_kmeans):
    df_test2 = df_X[df_X["Bankrupt?"] == 1]
    df_test2 = df_test2[df_test2["K-means"] == i]
    print(f"Cluster {i} : {len(df_test2.index)/len(df_test.index) * 100}")


print("Pourcentage d'entreprises en non faillites dans même cluster : ")
df_test_bis = df_X[df_X["Bankrupt?"] == 0]
for i in range(0, nombre_clusters_kmeans):
    df_test2_bis = df_X[df_X["Bankrupt?"] == 0]
    df_test2_bis = df_test2_bis[df_test2_bis["K-means"] == i]
    print(f"Cluster {i} : {len(df_test2_bis.index)/len(df_test_bis.index) * 100}")
