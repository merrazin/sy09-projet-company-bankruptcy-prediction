    Bankrupt?: A binary indicator (yes/no or 1/0) representing whether the company went bankrupt.

    ROA(C) before interest and depreciation before interest: Return on Assets (C) calculates how profitable a company's assets are in generating revenue before taking interest and depreciation into account.

    ROA(A) before interest and % after tax: Return on Assets (A) measures the profitability of a company's assets after taxes but before interest expenses are deducted.

    ROA(B) before interest and depreciation after tax: Return on Assets (B) is a profitability ratio that measures how well a company's assets generate profit after tax has been paid but before deducting interest and depreciation.

    Operating Gross Margin: This is the gross margin, expressed as a percentage, which is calculated without considering non-operating income and expenses.

    Realized Sales Gross Margin: Gross profit margin specifically from sales that have been completed or "realized."

    Operating Profit Rate: The ratio of operating profit to revenue, which indicates the efficiency of a company's core business without considering the effects of financing and taxes.

    Pre-tax net Interest Rate: The ratio of net interest expenses to pre-tax income, indicating how much of the company's earnings are used to cover interest expenses before taxes.

    After-tax net Interest Rate: This ratio indicates the proportion of net interest expenses to the company's after-tax income.

    Non-industry income and expenditure/revenue: Income and expenditure that do not arise from the company's primary business operations, considered in relation to the revenue.

    Continuous interest rate (after tax): An interest rate applied continuously to a company's after-tax income.

    Operating Expense Rate: The ratio of operating expenses to total revenue, indicating the cost of operating the primary business relative to the income generated.

    Research and development expense rate: The ratio of R&D expenses to total revenue.

    Cash flow rate: A general term which could refer to the rate at which cash flows into or out of the business, but it needs specific context to define precisely.

    Interest-bearing debt interest rate: The interest rate paid on debts that incur interest costs.

    Tax rate (A): The effective tax rate of the company, potentially a specific version (A) indicating a particular way of calculating it.

    Net Value Per Share (B), Net Value Per Share (A), Net Value Per Share (C): Various ways of calculating the net value (assets minus liabilities) attributable to a single share, which might include or exclude certain items.

    Persistent EPS in the Last Four Seasons: Earnings per share that have been consistent over the last four quarters.

    Cash Flow Per Share: The amount of cash flow generated per share of the company.

    Revenue Per Share (Yuan ¥): The total revenue divided by the number of outstanding shares, expressed in Yuan.

    Operating Profit Per Share (Yuan ¥): Operating income divided by the number of outstanding shares, in Yuan.

    Per Share Net profit before tax (Yuan ¥): The net profit before tax per outstanding share, in Yuan.

    Realized Sales Gross Profit Growth Rate: The growth rate of the gross profit from sales that have been realized.

    Operating Profit Growth Rate: The rate at which the company's operating profit is increasing.

    After-tax Net Profit Growth Rate: The growth rate of the company's net profit after taxes.

    Regular Net Profit Growth Rate: The growth rate of the net profit, typically considering only regular or recurring profit sources.

    Continuous Net Profit Growth Rate: The growth rate of net profits over a sustained period.

    Total Asset Growth Rate: The rate at which the company's total assets are growing.

    Net Value Growth Rate: The rate of growth of the net value (equity).

    Total Asset Return Growth Rate Ratio: The ratio showing the growth rate of returns generated from total assets.

    Cash Reinvestment %: The percentage of cash flow that is reinvested back into the company.

    Current Ratio: A liquidity ratio that measures a company's ability to pay short-term obligations with its current assets.

    Quick Ratio: A measure of a company's ability to meet its short-term obligations with its most liquid assets.

    Interest Expense Ratio: The ratio of interest expenses to total income, indicating how much income is used to pay interest.

    Total debt/Total net worth: A leverage ratio comparing the total debt to the total net worth (equity) of the company.

    Debt ratio %: A measure of the company's financial leverage, calculated as total liabilities divided by total assets.

    Net worth/Assets: The ratio of net worth (or equity) to total assets.

    Long-term fund suitability ratio (A): A ratio that may refer to the suitability of long-term funds to cover long-term needs, often comparing long-term sources of finance to the application of funds.

    Borrowing dependency: A measure of how much a company relies on borrowed funds.

    Contingent liabilities/Net worth: The ratio of potential liabilities that could occur based on the outcome of a future event to the net worth.

    Operating profit/Paid-in capital: The ratio of operating profit to the paid-in capital (total amount of capital funded by shareholders).

    Net profit before tax/Paid-in capital: The ratio of net profit before tax to paid-in capital.

    Inventory and accounts receivable/Net value: A financial ratio that compares the sum of inventory and accounts receivable to the company’s net value (equity).

    Total Asset Turnover: A measure of how effectively a company uses its assets to generate sales revenue.

    Accounts Receivable Turnover: How many times a company collects its average accounts receivable balance in a period.

    Average Collection Days: The average number of days it takes for a company to receive payment after a sale has been made.

    Inventory Turnover Rate (times): How many times a company’s inventory is sold and replaced over a period.

    Fixed Assets Turnover Frequency: How frequently a company's fixed assets are turned over, i.e., how efficiently they are used to generate sales.

    Net Worth Turnover Rate (times): How many times the company’s net worth is turned over in terms of sales.

    Revenue per person: Revenue generated per employee.

    Operating profit per person: Operating profit generated per employee.

    Allocation rate per person: The rate at which resources (possibly capital, expenses, or investments) are allocated per employee.

    Working Capital to Total Assets: A measure of the company’s efficiency, showing the proportion of working capital to total assets.

    Quick Assets/Total Assets: The proportion of assets that can be quickly converted into cash to the total assets.

    Current Assets/Total Assets: The ratio of current assets (those that can be converted into cash within a year) to total assets.

    Cash/Total Assets: The proportion of cash and cash equivalents to the company’s total assets.

    Quick Assets/Current Liability: The ratio of liquid assets to current liabilities.

    Cash/Current Liability: The ratio of cash and cash equivalents to current liabilities.

    Current Liability to Assets: The ratio of current liabilities to total assets.

    Operating Funds to Liability: The ratio of funds from operations to total liabilities.

    Inventory/Working Capital: The ratio of inventory to working capital.

    Inventory/Current Liability: The ratio of inventory to current liabilities.

    Current Liabilities/Liability: The ratio of current liabilities to total liabilities.

    Working Capital/Equity: The ratio of working capital to shareholders' equity.

    Current Liabilities/Equity: The ratio of current liabilities to shareholders' equity.

    Long-term Liability to Current Assets: The ratio of long-term liabilities to current assets.

    Retained Earnings to Total Assets: The ratio of retained earnings to the total assets of the company.

    Total income/Total expense: The ratio of total income to total expenses.

    Total expense/Assets: The ratio of total expenses to total assets.

    Current Asset Turnover Rate: How quickly current assets are converted into sales or cash.

    Quick Asset Turnover Rate: The rate at which quick assets are converted into sales.

    Working capital Turnover Rate: A measure of how effectively working capital is used for generating sales.

    Cash Turnover Rate: The rate at which cash is generated through sales.

    Cash Flow to Sales: The ratio of cash flow from operations to sales.

    Fixed Assets to Assets: The ratio of fixed assets to total assets.

    Current Liability to Liability: The ratio of current liabilities to total liabilities.

    Current Liability to Equity: The ratio of current liabilities to shareholders' equity.

    Equity to Long-term Liability: The ratio of shareholders' equity to long-term liabilities.

    Cash Flow to Total Assets: The ratio of cash flow from operations to total assets.

    Cash Flow to Liability: The ratio of cash flow from operations to total liabilities.

    CFO to Assets: The ratio of cash flow from operations (CFO) to total assets.

    Cash Flow to Equity: The ratio of cash flow