import pandas as pd
from sklearn.preprocessing import StandardScaler


df = pd.read_csv('data.csv')

df.columns = df.columns.str.strip()
df=df.drop(columns=['Net Income Flag','Liability-Assets Flag'],axis=1) # Drop the column with only one value
df_X=df.drop(columns=['Bankrupt?'],axis=1)
df_Y=df['Bankrupt?']

# normalize the data_X
scaler = StandardScaler()
df_X = pd.DataFrame(scaler.fit_transform(df_X), columns=df_X.columns)

iqr_threshold=0.07
iqr = df_X.quantile(0.75) - df_X.quantile(0.25)
iqr = iqr[iqr < iqr_threshold]
#print("number of columns to drop with 0.07 iqr threshhold",len(iqr))
#print(iqr.index.values.tolist())
list_of_columns_to_drop = iqr.index.values.tolist()
df_X = df_X.drop(columns=list_of_columns_to_drop)

col_drop_0_9_correlation=["ROA(A) before interest and % after tax",
'ROA(B) before interest and depreciation after tax',
'ROA(C) before interest and depreciation before interest',
'Realized Sales Gross Margin',
'Operating Gross Margin',
'Net Value Per Share (C)',
'Net Value Per Share (B)',
'Per Share Net profit before tax (Yuan ¥)',
'Persistent EPS in the Last Four Seasons',
'Operating Profit Per Share (Yuan ¥)',
'Debt ratio %',
'Borrowing dependency',
'Liability to Equity',
'Current Liability to Equity']
df_X = df_X.drop(columns=col_drop_0_9_correlation)

df_X.shape
df_Y.value_counts()




# exemple d'évaluation de la performance d'un modèle + cross validation + oversampling
# modèle de régression logistique

from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import LogisticRegression
from imblearn.over_sampling import SMOTE
import numpy as np
# Kfold with oversampling - SMOTE

K_fold = 10
skf = StratifiedKFold(n_splits=K_fold)


recall_list=[]
precision_list=[]
F_beta_2_list=[]
for train_indices,test_indices in skf.split(df_X, df_Y):
    df_X_train, df_X_test = df_X.iloc[train_indices], df_X.iloc[test_indices]
    df_Y_train, df_Y_test = df_Y.iloc[train_indices], df_Y.iloc[test_indices]
    # oversampling - SMOTE 
    # SMOTE - strategy = 1000 samples for the minority class
    strategy = { 1: 1000}
    sm = SMOTE(random_state=42,sampling_strategy=strategy)
    X_res, y_res = sm.fit_resample(df_X_train, df_Y_train)
    model = LogisticRegression(solver='liblinear')
    model.fit(X_res, y_res)
    #####################################
        # evaluation sur un fold (qui peut être généralisé sur un train-test split classique)
    #####################################
    y_pred = model.predict(df_X_test)
    precision=precision_recall_fscore_support(df_Y_test, y_pred,beta=1.5)[0][1]
    recall=precision_recall_fscore_support(df_Y_test, y_pred,beta=1.5)[1][1]
    F_beta_2=precision_recall_fscore_support(df_Y_test, y_pred,beta=1.5)[2][1]
    
    recall_list.append(recall)
    precision_list.append(precision)
    F_beta_2_list.append(F_beta_2)

    ######## result verification (calcul des métriques à la main)  à supprimer ########
    #print(confusion_matrix(df_Y_test, y_pred))
    recall_test=confusion_matrix(df_Y_test, y_pred)[1][1]/(confusion_matrix(df_Y_test, y_pred)[1][1]+confusion_matrix(df_Y_test, y_pred)[1][0])
    precision_test=confusion_matrix(df_Y_test, y_pred)[1][1]/(confusion_matrix(df_Y_test, y_pred)[1][1]+confusion_matrix(df_Y_test, y_pred)[0][1])
    beta=2
    f2_beta=(1+beta**2)*precision_test*recall_test/((beta**2)*precision_test+recall_test)
    #print("recall_test",recall_test,recall)
    #print("precision_test",precision_test,precision)
    #print("f2_beta",f2_beta,F_beta_2)
    #print(classification_report(df_Y_test, y_pred))
    #print("\n\n")
    #print("-------------------")
    
# Résultat de la cross validation / regression logistique avec un oversampling SMOTE
print("recall",np.mean(recall_list))
print("precision",np.mean(precision_list))
print("F_beta_2",np.mean(F_beta_2_list)) 