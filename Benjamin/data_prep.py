import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold, train_test_split
from sklearn.linear_model import LogisticRegression
from imblearn.over_sampling import SMOTE
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.decomposition import PCA
import numpy as np


def data_preparation(data_path="data.csv", get_all_columns=False, get_acp = False):
    df = pd.read_csv(data_path)

    df.columns = df.columns.str.strip()
    df=df.drop(columns=['Net Income Flag','Liability-Assets Flag'],axis=1) # Drop the column with only one value
    df_X=df.drop(columns=['Bankrupt?'],axis=1)
    df_Y=df['Bankrupt?']

    # normalize the data_X
    scaler = StandardScaler()
    df_X = pd.DataFrame(scaler.fit_transform(df_X), columns=df_X.columns)



    if not get_all_columns:
        col_to_drop=['ROA(C) before interest and depreciation before interest',
        'ROA(B) before interest and depreciation after tax',
        'ROA(A) before interest and % after tax',
        'Gross Profit to Sales',
        'Realized Sales Gross Margin',
        'Pre-tax net Interest Rate',
        'Operating Profit Rate',
        'Continuous interest rate (after tax)',
        'Net Value Per Share (C)',
        'Net Value Per Share (B)',
        
        'Persistent EPS in the Last Four Seasons',
        'Per Share Net profit before tax (Yuan ¥)',
        'Operating Profit Per Share (Yuan ¥)',
        

        'Regular Net Profit Growth Rate',
        
        'Debt ratio %',
        'Current Liability to Equity',
        'Current Liabilities/Equity',
        'Borrowing dependency',
        'Cash Flow to Sales']
        col_to_drop += ['Operating Expense Rate', 'Accounts Receivable Turnover', 'Average Collection Days', 'Inventory Turnover Rate (times)', 'Net Worth Turnover Rate (times)', 'Revenue per person', 'Current Liabilities/Liability', 'Cash Turnover Rate', 'Current Liability to Liability']
        df_X = df_X.drop(columns=col_to_drop)


    if get_acp :
        df_X, df_Y = get_acp_scaled(df_X, df_Y, nb_cp=11)
    
    # Assume X is your feature matrix and y is your target vector
    # Define the random state for reproducibility
    random_state = 42

    # Perform the stratified split
    X_train, X_test, y_train, y_test = train_test_split(
        df_X, df_Y, 
        test_size=0.1,  # Adjust the test size as needed
        stratify=df_Y,     # This ensures the stratified split
        random_state=random_state  # Set the random state for reproducibility
    )


    return X_train, X_test, y_train, y_test

def get_acp(df_X, df_Y, nb_cp=10):
    n_comp=40
    pca = PCA(n_components=n_comp)
    pca.fit(df_X)
    X = pca.transform(df_X)[:,:nb_cp]
    return pd.DataFrame(X), df_Y


def get_acp_scaled(df_X, df_Y, nb_cp=10):
    ########## PONDERATION DES DONNEES #########
    # on pourrait utiliser des techniques de pondération
    n=len(df_Y)

    weight_bankrupt=0.5/220
    weight_Non_bankrupt=0.5/6599
    weights=np.where(df_Y==1,weight_bankrupt,weight_Non_bankrupt)
    scaled_X = df_X.multiply(np.sqrt(weights), axis=0)
    n_comp=40
    pca = PCA(n_components=n_comp)
    pca.fit(scaled_X)
    pca_X = pca.transform(df_X)
    X = pca_X[:,:nb_cp]
    Y = df_Y
    # plt.figure(figsize=(10, 6))
    # sns.barplot(x=np.arange(1, n_comp+1), y=cumulative_variance_scaled, palette="viridis")
    # plt.plot(np.arange(0, n_comp), cumulative_variance_scaled, marker='o', linestyle='-', color='black')
    # plt.title('Explained Variance by Components')
    # plt.xlabel('Number of Components')
    # plt.ylabel('Explained Variance Ratio')
    # plt.plot()
    # print(cumulative_variance_scaled)
    return pd.DataFrame(X), Y


if __name__ == '__main__':

    # exemple d'évaluation de la performance d'un modèle + cross validation + oversampling
    # modèle de régression logistique

    df_X, df_X_global_test, df_Y, df_Y_global_test = data_preparation(get_all_columns=True, get_acp=True)
    # df_X, df_Y = get_acp_scaled(df_X, df_Y, nb_cp=8)


    print("Shape df_X : ", df_X.shape)


    # Kfold with oversampling - SMOTE

    K_fold = 10
    skf = StratifiedKFold(n_splits=K_fold)


    recall_list=[]
    precision_list=[]
    F_beta_2_list=[]
    for train_indices,test_indices in skf.split(df_X, df_Y):
        df_X_train, df_X_test = df_X.iloc[train_indices], df_X.iloc[test_indices]
        df_Y_train, df_Y_test = df_Y.iloc[train_indices], df_Y.iloc[test_indices]
        # oversampling - SMOTE 
        # SMOTE - strategy = 1000 samples for the minority class
        strategy = { 1: 1000}
        sm = SMOTE(random_state=42,sampling_strategy=strategy)
        X_res, y_res = sm.fit_resample(df_X_train, df_Y_train)
        print("Shape X_res : ", X_res.shape)
        print("Shape X_train : ", df_X_train.shape)
        model = make_pipeline(PolynomialFeatures(degree=2), LogisticRegression(solver='liblinear', max_iter=2000))
        # model = LogisticRegression(solver='liblinear')
        model.fit(X_res, y_res)
        #####################################
            # evaluation sur un fold (qui peut être généralisé sur un train-test split classique)
        #####################################
        y_pred = model.predict(df_X_test)
        precision=precision_recall_fscore_support(df_Y_test, y_pred,beta=1.5)[0][1]
        recall=precision_recall_fscore_support(df_Y_test, y_pred,beta=1.5)[1][1]
        F_beta_2=precision_recall_fscore_support(df_Y_test, y_pred,beta=1.5)[2][1]
        
        recall_list.append(recall)
        precision_list.append(precision)
        F_beta_2_list.append(F_beta_2)

        
    # Résultat de la cross validation / regression logistique avec un oversampling SMOTE
    print("recall",np.mean(recall_list))
    print("precision",np.mean(precision_list))
    print("F_beta_2",np.mean(F_beta_2_list)) 

    print('--------------Test sur le jeu de test global ----------------')

    strategy = { 1: 1100}
    sm = SMOTE(random_state=42,sampling_strategy=strategy)
    X_res, y_res = sm.fit_resample(df_X, df_Y)

    # model = LogisticRegression(solver='liblinear')
    model = make_pipeline(PolynomialFeatures(degree=2), LogisticRegression(solver='liblinear', max_iter=2000))
    model.fit(X_res, y_res)

    y_pred = model.predict(df_X_global_test)
    precision=precision_recall_fscore_support(df_Y_global_test, y_pred,beta=1.5)[0][1]
    recall=precision_recall_fscore_support(df_Y_global_test, y_pred,beta=1.5)[1][1]
    F_beta_2=precision_recall_fscore_support(df_Y_global_test, y_pred,beta=1.5)[2][1]

    print("recall",recall)
    print("precision",precision)
    print("F_beta_2",F_beta_2) 