import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA


def scatterplot_pca(
    columns=None, hue=None, style=None, data=None, pc1=1, pc2=2, **kwargs
):
    """Diagramme de dispersion dans le premier plan principal.

    Permet d'afficher un diagramme de dispersion lorsque les données
    ont plus de deux dimensions. L'argument `columns` spécifie la
    liste des colonnes à utiliser pour la PCA dans le jeu de données
    `data`. Les arguments `style` et `hue` permettent de spécifier la
    forme et la couleur des marqueurs. Les arguments `pc1` et `pc2`
    permettent de sélectionner les composantes principales (par défaut
    la première et deuxième). Retourne l'objet `Axes` ainsi que le
    modèle `PCA` utilisé pour réduire la dimension.

    :param columns: Les colonnes quantitatives de `data` à utiliser
    :param hue: La colonne de coloration
    :param style: La colonne du style
    :param data: Le dataFrame Pandas
    :param pc1: La composante en abscisse
    :param pc2: La composante en ordonnée

    """
     # Select relevant columns (should be numeric)
    data_quant = data if columns is None else data[columns]
    data_quant = data_quant.drop(
        columns=[e for e in [hue, style] if e is not None], errors="ignore"
    )

    # Reduce to two dimensions if needed
    if data_quant.shape[1] == 2:
        data_pca = data_quant
        pca = None
    else:
        n_components = max(pc1, pc2)
        pca = PCA(n_components=n_components)
        data_pca = pca.fit_transform(data_quant)
        data_pca = pd.DataFrame(
            data_pca[:, [pc1 - 1, pc2 - 1]], columns=[f"PC{pc1}", f"PC{pc2}"]
        )

    # Keep name, force categorical data for hue and steal index to
    # avoid unwanted alignment
    if isinstance(hue, pd.Series):
        if not hue.name:
            hue.name = "hue"
        hue_name = hue.name
    elif isinstance(hue, str):
        hue_name = hue
        hue = data[hue]
    elif isinstance(hue, np.ndarray):
        hue = pd.Series(hue, name="class")
        hue_name = "class"

    hue = hue.astype("category")
    hue.index = data_pca.index
    hue.name = hue_name

    if isinstance(style, pd.Series):
        if not style.name:
            style.name = "style"
        style_name = style.name
    elif isinstance(style, str):
        style_name = style
        style = data[style]
    elif isinstance(style, np.ndarray):
        style = pd.Series(style, name="style")
        style_name = "style"

    full_data = data_pca
    if hue is not None:
        full_data = pd.concat((full_data, hue), axis=1)
        kwargs["hue"] = hue_name
    if style is not None:
        full_data = pd.concat((full_data, style), axis=1)
        kwargs["style"] = style_name

    x, y = data_pca.columns
    ax = sns.scatterplot(x=x, y=y, data=full_data, **kwargs)

    return ax, pca

def filter_valeurs_aberrantes(data_csv, nb_bankrupt_max = 2):
    """
    A utiliser directement à la lecture du csv.
    Lit le csv et supprime les valeurs aberrantes. Supprime aussi les colonnes "Net Income Flag" et "Liability-Assets Flag".
    Les valeurs aberrantes sont les valeurs pour lesquelles Value - Quantile95 > 10000. Seuls les individus faisant partie de variables pour lesquelles moins de "nb_bankrupt_max" individus avec bankrupt n'est considéré comme aberrant sont supprimés.
    Retire aussi l'individu si toutes les valeurs au dessus de 1 dans une variables sont supérieures à threshold + quantile(0.95) (variables pour lesquelles seul une poignée d'individus sont totalement aberrants par rapport au reste)
    """
    if type(data_csv) == str:
        X = pd.read_csv(data_csv)
        X.columns = X.columns.str.strip()

        X = X.drop(columns=["Net Income Flag", "Liability-Assets Flag"])
    else : 
        X = data_csv

    #Detection des outliers quand valeur > quantile95% + 10000
    filtered_X = X.copy()
    threshold = 10000

    columns_aberrantes = []
    outlier_data = []

    for col_name in X.columns : 
        quantile = np.quantile(X[col_name], 0.95)
        ecart_max_q = max(X[col_name]) - quantile
        if ecart_max_q > threshold:
            # print("La colonne \"", col_name, "\" contient possiblement une valeur aberrante : max - quantile(0.95) = ", ecart_max_q)
            # print(" - Nombre de valeurs aberrantes : ", X[col_name][X[col_name] > quantile + threshold].shape)
            # print(" - Nombre de valeurs supérieures à 1 : ", X[col_name][X[col_name] > 1].shape)
            # print(" - Nombre de valeurs aberrantes avec Bankrupt=1 : ", X.loc[(X[col_name] > quantile + threshold) & (X["Bankrupt?"] == 1)].shape[0])
            # 0 == X.loc[X.loc[X[col_name] > quantile + threshold]["Bankrupt?"] == 1].shape[0]

            if X.loc[(X[col_name] > quantile + threshold) & (X["Bankrupt?"] == 1)].shape[0] <= nb_bankrupt_max or X[col_name][X[col_name] > quantile + threshold].shape == X[col_name][X[col_name] > 1].shape :
                columns_aberrantes.append(col_name)
                outlier_data.extend(filtered_X.loc[filtered_X[col_name] - quantile > threshold].to_dict(orient='records'))
                filtered_X = filtered_X.loc[filtered_X[col_name] - quantile <= threshold]
            
    outlier_df = pd.DataFrame(outlier_data)    

    print("Nombre de colonnes aberrantes : ", len(columns_aberrantes))
    print("Nombre de lignes restantes : ",  filtered_X.shape[0])
    print("Nombre de lignes supprimées : ", filtered_X.shape[0] - X.shape[0])
    print("Nombre de lignes supprimées avec bankrupt == 1 : ", outlier_df.loc[outlier_df["Bankrupt?"] == 1].shape[0])

    return filtered_X, outlier_df

def remove_heavilyCorr_normalize_df(df):
    scaler = StandardScaler()
    df.columns = df.columns.str.strip()
    try :
        df=df.drop(columns=['Net Income Flag','Liability-Assets Flag'],axis=1) # Drop the column with only one value
    except Exception as e:
        print("Colonnes qualitative déjà retirées")
    df_X = df.drop(columns=['Bankrupt?'])
    df_X = pd.DataFrame(scaler.fit_transform(df_X), columns=df_X.columns)
    df_y = df['Bankrupt?']
    cols_remove=['ROA(A) before interest and % after tax',
 'Net Value Per Share (A)',
 'Operating Profit Per Share (Yuan ¥)',
 'ROA(B) before interest and depreciation after tax',
 'Per Share Net profit before tax (Yuan ¥)',
 'Operating profit/Paid-in capital',
 'Net Value Per Share (C)',
 'Persistent EPS in the Last Four Seasons',
 'ROA(C) before interest and depreciation before interest',
 'Net Value Per Share (B)',
 'Net profit before tax/Paid-in capital',
 'Retained Earnings to Total Assets',
 'Operating Gross Margin',
 'Realized Sales Gross Margin',
 'Continuous interest rate (after tax)',
 'After-tax net Interest Rate',
 'Pre-tax net Interest Rate',
 'Non-industry income and expenditure/revenue',
 'Working capitcal Turnover Rate',
 'Cash Reinvestment %',
 'Cash Flow Per Share',
 'Operating Funds to Liability',
 'CFO to Assets',
 'After-tax Net Profit Growth Rate',
 'Current Liability to Assets',
 'Debt ratio %',
 'Inventory and accounts receivable/Net value',
 'Current Liabilities/Equity',
 'Borrowing dependency',
 'Current Liability to Equity',
 "Net Income to Stockholder's Equity",
 'Equity to Long-term Liability',
 'Contingent liabilities/Net worth',
 'Net Worth Turnover Rate (times)',
 'Working Capital to Total Assets',
 'Quick Assets/Total Assets',
 'Current Liabilities/Liability',
 'Cash Flow to Liability']
    df_X=df_X.drop(columns=cols_remove,axis=1)
    return df_X,df_y


if __name__ == '__main__' :
    filter_valeurs_aberrantes('data.csv')